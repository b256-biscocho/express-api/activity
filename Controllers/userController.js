const User = require("../Models/User.js");
const Product = require("../Models/Product.js")
const bcrypt = require("bcrypt");
const auth = require("../auth.js");


module.exports.checkIfEmailExists = (requestBody) => {

	// The result is sent back to the frontend via the "then" method found in the route file
	return User.find({email: requestBody.email}).then(result => {

		// The "find" method returns a record if a match is found
		if(result.length > 0) {

			return true;

		// No duplicate email found
		// The user is not yet registered in the database
		} else {

			return false;

		}
	})
};


module.exports.getProfile = (data) => {

	// findById(1)
	return User.findById(data.userId).then(result => {

		// Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
		result.password = "";

		// Returns the user information with the password as an empty string
		return result;

	});

};





// User Registration

module.exports.registerUser = (requestBody) => {

	let newUser = new User({
		email: requestBody.email,
		password: bcrypt.hashSync(requestBody.password, 10)
	})

	return newUser.save().then((user, err) => {

		// User registration failed
		if(err) {

			return false;

		// User registration successful
		} else {

			return true;

		}
	})
} 

// User authentication

module.exports.authenticateUser = (requestBody) => {

	return User.findOne({email: requestBody.email}).then(result => {


		if(result == null) {

			return false;

		} else {

			const isPasswordCorrect = bcrypt.compareSync(requestBody.password, result.password)

			if(isPasswordCorrect) {

				return {access: auth.createAccessToken(result)}

			} else {

				return false;
			};
		};
	});
};





//Creating an Order (non-admin only) 


module.exports.createOrder = async (data) => {

let price = await Product.findById(data.productId).then(product => {
					return product.price
				});
let totalProductAmount = price * data.quantity;


	let isUserUpdated = await User.findById(data.userId).then(user => {
		
	if (!user.orderedProducts) {
      user.orderedProducts = []; // Initialize orderedProducts as an empty array
    }

    user.orderedProducts.push({
      productId: data.productId,
      productName: data.productName,
      quantity: data.quantity,
      totalAmount: totalProductAmount, // Correct the variable name here
    });

		/*		
		user.orderedProducts.push({
			
				productId: data.productId,
				productName: data.productName,
				quantity: data.quantity,
				totalAmount: totalAmount

		});
		*/


		return user.save().then((checkedOut, err) => {

			if(err) {

				return false;

			} else {

				return true

			}
		})
	});

	let isProductUpdated = await Product.findById(data.productId).then(product => {

		product.userOrders.push({
			userId: data.userId
		});

		return product.save().then((ordered, err) => {

			if(err) {

				return false;

			} else {

				return true;

			}
		})
	});

	if(isUserUpdated && isProductUpdated) {

		return true;

	} else {

		return false;

	}
}





// Get all users
module.exports.allUsers = () => {

	return User.find({}).then(result => {

		return result;
	})
}



//Getting non-admin users
module.exports.getNonAdmin = () =>{
	return User.find({isAdmin: false}).then(result => {
		return result;
	})
}

//Set as admin


module.exports.makeAdmin = (reqParams) => {

	let isActiveUpdated = {
		isAdmin: true
	}

	// 
	return User.findByIdAndUpdate(reqParams.userId, isActiveUpdated).then((result, err) => {

		if(err) {

			return false;

		} else {

			return true;
		}
	})
}


