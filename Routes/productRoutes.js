const express = require("express");
const router = express.Router();
const productController = require("../Controllers/productController.js");
const auth = require("../auth.js");

// Route for creating a product by admin
router.post("/create", auth.verify, (req, res) => {

	// Check if the user making the request is an admin
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	} 

	if(data.isAdmin) {

		productController.addProduct(data.product).then(resultFromController => res.send(resultFromController));


	} else {
		res.send(false);
	}
});

//Route for retrieving ALL courses
router.get("/all", (req,res)=>{

	productController.getAllProducts().then(resultFromController => res.send(resultFromController));

})




//Route for retrieving all active products
router.get("/active", (req,res)=>{
	productController.getActiveProducts().then(resultFromController => res.send(resultFromController));
})



//Route for retrieving specific product
router.get("/:productId", (req,res)=>{
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
})



//Route for update product info(admin only)

/*
 router.put("/update/:productId", auth.verify, (req, res) => {

 	const data = {

 		product: req.body,
 		isAdmin: auth.decode(req.headers.authorization).isAdmin,
 		params: req.params
 	}

 	if (data.isAdmin) {

 		productController.updateProduct(data.product, data.params).then(resultFromController => res.send(resultFromController));

 	} else {

 		res.send(false);
 	}

 })
*/
// ROute to update product (admin only)

 router.patch("/update/:productId", auth.verify, (req, res) => {

 	const data = {

 		product: req.body,
 		isAdmin: auth.decode(req.headers.authorization).isAdmin,
 		params: req.params
 	}

 	if (data.isAdmin) {

 		productController.updateProduct(data.product, data.params).then(resultFromController => res.send(resultFromController));

 	} else {

 		res.send(false);
 	}

 })



/*router.put("/update/:productId", auth.verify, (req, res)=>{
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		params: req.params
	}
	if (data.isAdmin){
	productController.updateProduct(data.product, data.params).then(resultFromController => res.send(resultFromController));	
	} else{
		res.send(false);
	}
})
*/

//Route to archiving a product (admin only)
router.put("/archive/:productId", auth.verify, (req, res) => {

	const data ={
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		params: req.params
	}

	if(data.isAdmin) {

		productController.archiveProduct(data.params).then(resultFromController => res.send(resultFromController));
	
	} else {

		res.send(false);

	}
});



// Reactivate

 router.put("/reactivate/:productId", auth.verify, (req, res) => {

    const data = {

        isAdmin: auth.decode(req.headers.authorization).isAdmin,
        params: req.params
    }

    if (data.isAdmin) {

        productController.reactivateProduct(data.params).then(resultFromController => res.send(resultFromController));
    
    } else {

        res.send(false);
    }
 })


 module.exports = router;


module.exports = router;